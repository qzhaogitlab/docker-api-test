require 'http'

def check_url
  begin
    yield
  rescue StandardError
    false
  end
end

def check_url_alive(url)
  check_url do
    ret_code = HTTP.get(url).code
    ret_code == 200
  end
end

def status_ok?(hash)
  return false unless hash.is_a?(Hash) && !hash.empty?

  status_ok = true
  hash.each do |key, value|
    status_ok = false unless value.is_a?(Hash) && value['status'] == 'ok'
  end

  status_ok
end

def old_status_ok?(hash, keys)
  return false unless hash.is_a?(Hash) && !hash.empty?
  
  status_ok = true
  keys.each do |key|
    status_ok = false unless hash && hash[key] && hash[key]['status'] == 'ok'
  end
  
  status_ok
end

def check_gitlab_ready(url)
  check_url do
    response = JSON.parse(HTTP.get(url))
    status_ok?(response)
  end
end

# wait until Gitlab started
gitlab_started = false
gitlab_ready = false
360.times do |i|
  gitlab_started ||= check_url_alive("http://docker/api/v4/groups")
  gitlab_ready ||= check_gitlab_ready("http://docker/-/readiness")
  
  break if gitlab_started && gitlab_ready

  sleep(1)
end
      
abort 'Gitlab services failed to start within 6 minutes' unless gitlab_started && gitlab_ready
      
puts 'Gitlab started'