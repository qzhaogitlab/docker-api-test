require 'docker'

image_reference = 'gitlab/gitlab-ce:latest'

Docker::Image.create('fromImage' => image_reference)
container = Docker::Container.create(
  'name' => 'gitlab',
  'detach' => true,
  'Env' => ["GITLAB_OMNIBUS_CONFIG=gitlab_rails['monitoring_whitelist'] = ['0.0.0.0/0'];" ],
  'Image' => image_reference,
  'HostConfig' => {
    'PortBindings' => {
      '80/tcp' => [{ 'HostPort' => '80' }],
      '443/tcp' => [{ 'HostPort' => '443' }]
    }
  }
)

container.start

#cmd_update_monitoring_whitelist = ["bash", "-c", "echo \"gitlab_rails['monitoring_whitelist'] = ['0.0.0.0/0']\" >> /etc/gitlab/gitlab.rb" ]
#stdout, stderr, code = container.exec(cmd_update_monitoring_whitelist)
#abort stderr.to_s unless code.zero?

#container.restart
